#ifndef __SQSFFONT_H
#define __SQSFFONT_H

#include <SFML/Graphics/Font.hpp>

static SQInteger sqsfFont_release( SQUserPointer p, SQInteger size ) {
	sf::Font* fnt = (sf::Font*)p; (void)size;
	delete fnt;
	return 1;
}

static SQInteger sqsfFont_construct( HSQUIRRELVM v ) {
	sf::Font* fnt = new sf::Font();

	if ( sq_gettop( v ) > 1 ) {
		const SQChar* s;
		sq_getstring( v, 2, &s );
		fnt->loadFromFile( std::string( s ) );
	}

	sq_setinstanceup( v, 1, fnt );
	sq_setreleasehook( v, 1, sqsfFont_release );
	return 0;
}

static SQInteger sqsfFont_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfFont"), -1 );
	return 1;
}

#define GET_SELF() sf::Font* self = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&self, 0 )

#undef GET_SELF

static SQRegFunction sqsfFont_RegFunctions[] = {
	{_SC("constructor"), sqsfFont_construct, -1, _SC(".s")},
	{_SC("_typeof"), sqsfFont_typeof, 1, _SC("x")},
	{0, 0, 0, 0}
};

namespace sqsf {

static void Font_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfFont"), -1 );
	sq_newclass( v, SQFalse );
	sq_register_functions( v, sqsfFont_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}

#endif
