#include "sqsfGlobal.h"
#include "sqsfVertexArray.h"

namespace sqsf {

static SQInteger VertexArray_release( SQUserPointer p, SQInteger size ) {
	VertexArray* svxa = (VertexArray*)p; (void)size;
	delete svxa->vxa;
	sq_free( svxa, sizeof (VertexArray) );
	return 1;
}

static SQInteger VertexArray_construct( HSQUIRRELVM v ) {
	SQInteger pt = sf::Points;
	SQInteger sz = 0;

	if ( sq_gettop( v ) > 1 ) {
		sq_getinteger( v, 2, &pt );
		if ( sq_gettop( v ) > 2 ) {
			sq_getinteger( v, 3, &sz );
		}
	}

	VertexArray* svxa = (VertexArray*)sq_malloc( sizeof (VertexArray) );
	svxa->vxa = new sf::VertexArray( (sf::PrimitiveType)pt, sz );
	svxa->dra = (sf::Drawable*)svxa->vxa;
	svxa->tfa = NULL; // oh no

	sq_setinstanceup( v, 1, svxa );
	sq_setreleasehook( v, 1, VertexArray_release );
	return 0;
}

static SQInteger VertexArray_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfVertexArray"), -1 );
	return 1;
}

#define GET_SELF() VertexArray* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::VertexArray* self = _s->vxa

static SQInteger VertexArray_get( HSQUIRRELVM v ) {
	GET_SELF();
	SQInteger ix = 0;

	if ( sq_gettype( v, 2 ) == OT_INTEGER ) {
		sq_getinteger( v, 2, &ix );

		if ( (unsigned)ix < self->getVertexCount() ) {
			//sf::VertexArray va = *self;
			//sf::Vertex vt = va[ix];
			sqsf_pushvertex( v, (*self)[ix] );
		} else {
			sq_pushnull( v );
			return sq_throwobject( v );
		}
	} else {
		sq_pushnull( v );
		return sq_throwobject( v );
	}

	return 1;
}

static SQInteger VertexArray_set( HSQUIRRELVM v ) {
	GET_SELF();
	SQInteger ix = 0;

	if ( sq_gettype( v, 2 ) == OT_INTEGER ) {
		sq_getinteger( v, 2, &ix );
		if ( sq_gettype( v, 3 ) == OT_INSTANCE ) {
			VALIDATE_INSTANCEOF( v, 3, _SC("sfVertex") );

			if ( (unsigned)ix < self->getVertexCount() ) {
				//sf::VertexArray va = *self;
				sf::Vertex vt;
				sqsf_getvertex( v, 3, vt );
				(*self)[ix] = vt;
			}
		} else {
			return sq_throwerror( v, "Attemped to set integer index to something that is not an sfVertex instance." );
		}
	}

	sq_push( v, 3 );

	return 1;
}

static SQInteger VertexArray_append( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfVertex") );

	sf::Vertex vt;
	sqsf_getvertex( v, 2, vt );

	self->append( vt );

	return 0;
}

static SQInteger VertexArray_clear( HSQUIRRELVM v ) {
	GET_SELF();
	self->clear();
	return 0;
}

static SQInteger VertexArray_getVertexCount( HSQUIRRELVM v ) {
	GET_SELF();
	sq_pushinteger( v, self->getVertexCount() );
	return 1;
}

static SQInteger VertexArray_resize( HSQUIRRELVM v ) {
	GET_SELF();
	SQInteger nsz = 0;
	sq_getinteger( v, 2, &nsz );
	self->resize( nsz );
	return 0;
}

#undef GET_SELF

static SQRegFunction VertexArray_RegFunctions[] = {
	{_SC("constructor"), VertexArray_construct, -1, _SC(".ii")},
	{_SC("_typeof"), VertexArray_typeof, 1, _SC("x")},
	{_SC("_get"), VertexArray_get, 2, _SC("x.")},
	{_SC("_set"), VertexArray_set, 3, _SC("x..")},
	{_SC("append"), VertexArray_append, 2, _SC("xx")},
	{_SC("clear"), VertexArray_clear, 1, _SC("x")},
	{_SC("getVertexCount"), VertexArray_getVertexCount, 1, _SC("x")},
	{_SC("resize"), VertexArray_resize, 2, _SC("xi")},
	{0, 0, 0, 0}
};

void VertexArray_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfVertexArray"), -1 );
	sq_pushstring( v, _SC("sfDrawable"), -1 );
	sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	sq_register_functions( v, VertexArray_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
