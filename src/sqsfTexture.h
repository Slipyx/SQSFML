#ifndef __SQSFTEXTURE_H
#define __SQSFTEXTURE_H

#include <squirrel.h>

namespace sqsf {

void Texture_register( HSQUIRRELVM v );

}

#endif
