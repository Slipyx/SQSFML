#include "sqsfGlobal.h"
#include "sqsfWindow.h"

namespace sqsf {

static SQInteger Window_release( SQUserPointer p, SQInteger size ) {
	Window* win = (Window*)p; (void)size;
	delete win->rwin;
	sq_free( win, sizeof (Window) );
	return 1;
}

static SQInteger Window_construct( HSQUIRRELVM v ) {
	sf::VideoMode vMode;
	const SQChar* title = NULL;
	SQInteger style = sf::Style::Default;
	sf::ContextSettings ctxSets( 24, 0, 0, 2, 1, 0 );
	Window* win = (Window*)sq_malloc( sizeof (Window) );

	VALIDATE_INSTANCEOF( v, 2, _SC("sfVideoMode") );
	sq_pushstring( v, _SC("width"), -1 ); sq_get( v, 2 );
	sq_pushstring( v, _SC("height"), -1 ); sq_get( v, 2 );
	sq_pushstring( v, _SC("bitsPerPixel"), -1 ); sq_get( v, 2 );
	sq_getinteger( v, -3, (SQInteger*)&vMode.width );
	sq_getinteger( v, -2, (SQInteger*)&vMode.height );
	sq_getinteger( v, -1, (SQInteger*)&vMode.bitsPerPixel );
	sq_pop( v, 3 );
	sq_getstring( v, 3, &title );
	if ( sq_gettop( v ) > 3 ) sq_getinteger( v, 4, &style );

	win->rwin = new sf::RenderWindow( vMode, title, style, ctxSets );
	win->rtar = (sf::RenderTarget*)win->rwin;

	sq_setinstanceup( v, 1, win );
	sq_setreleasehook( v, 1, Window_release );
	return 0;
}

static SQInteger Window_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfWindow"), -1 );
	return 1;
}

#define GET_SELF() Window* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::RenderWindow* self = _s->rwin

static SQInteger Window_isOpen( HSQUIRRELVM v ) {
	GET_SELF();
	sq_pushbool( v, self->isOpen() );
	return 1;
}

static SQInteger Window_close( HSQUIRRELVM v ) {
	GET_SELF();
	self->close();
	return 0;
}

static SQInteger Window_display( HSQUIRRELVM v ) {
	GET_SELF();
	self->display();
	return 0;
}

static SQInteger Window_pollEvent( HSQUIRRELVM v ) {
	bool revt = false;
	sf::Event evt;
	GET_SELF();
	revt = self->pollEvent( evt );

	sq_pushstring( v, _SC("type"), -1 );
	sq_pushinteger( v, evt.type );
	sq_newslot( v, 2, SQFalse );

	// types
	if ( evt.type == sf::Event::TextEntered ) {
		sq_pushstring( v, _SC("text"), -1 );
		sq_pushinteger( v, evt.text.unicode );
		sq_newslot( v, 2, SQFalse );
	} else if ( evt.type == sf::Event::KeyPressed || evt.type == sf::Event::KeyReleased ) {
		sq_pushstring( v, _SC("key"), -1 );
		sq_newtable( v );
		sq_pushstring( v, _SC("code"), -1 );
		sq_pushinteger( v, evt.key.code );
		sq_newslot( v, -3, SQFalse );
		sq_newslot( v, 2, SQFalse );
	} else if ( evt.type == sf::Event::MouseMoved ) {
		sq_pushstring( v, _SC("mouseMove"), -1 );
		sqsf_pushvector( v, sf::Vector2f( evt.mouseMove.x, evt.mouseMove.y ) );
		sq_newslot( v, 2, SQFalse );
	} else if ( evt.type == sf::Event::Resized ) {
		sq_pushstring( v, _SC("size"), -1 );
		sq_newtable( v );
		sq_pushstring( v, _SC("width"), -1 ); sq_pushinteger( v, evt.size.width );
		sq_newslot( v, -3, SQFalse );
		sq_pushstring( v, _SC("height"), -1 ); sq_pushinteger( v, evt.size.height );
		sq_newslot( v, -3, SQFalse );
		sq_newslot( v, 2, SQFalse );
	}

	sq_pushbool( v, revt );
	return 1;
}

static SQInteger Window_setVerticalSyncEnabled( HSQUIRRELVM v ) {
	SQBool bEnable = SQFalse;
	GET_SELF();
	sq_getbool( v, 2, &bEnable );
	self->setVerticalSyncEnabled( bEnable );
	return 0;
}

static SQInteger Window_setFramerateLimit( HSQUIRRELVM v ) {
	GET_SELF();
	SQInteger frl = 0;
	sq_getinteger( v, 2, &frl );
	self->setFramerateLimit( frl );
	return 0;
}

static SQInteger Window_setSize( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );
	sf::Vector2f nsz;
	sqsf_getvector( v, 2, nsz );
	self->setSize( sf::Vector2u( nsz.x, nsz.y ) );
	return 0;
}

static SQInteger Window_setPosition( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );
	sf::Vector2f npos;
	sqsf_getvector( v, 2, npos );
	self->setPosition( sf::Vector2i( npos.x, npos.y ) );

	return 0;
}

#undef GET_SELF

static SQRegFunction Window_RegFunctions[] = {
	{_SC("constructor"), Window_construct, -3, _SC(".xsia")},
	{_SC("_typeof"), Window_typeof, 1, _SC("x")},
	{_SC("isOpen"), Window_isOpen, 1, _SC("x")},
	{_SC("close"), Window_close, 1, _SC("x")},
	{_SC("display"), Window_display, 1, _SC("x")},
	{_SC("pollEvent"), Window_pollEvent, 2, _SC("xt")},
	{_SC("setVerticalSyncEnabled"), Window_setVerticalSyncEnabled, 2, _SC("xb")},
	{_SC("setFramerateLimit"), Window_setFramerateLimit, 2, _SC("xi")},
	{_SC("setSize"), Window_setSize, 2, _SC("xx")},
	{_SC("setPosition"), Window_setPosition, 2, _SC("xx")},
	{0, 0, 0, 0}
};

// ===========
// sfVideoMode
static SQInteger VideoMode_getDesktopMode( HSQUIRRELVM v ) {
	sf::VideoMode dtvm = sf::VideoMode::getDesktopMode();
	sq_pushroottable( v );
	sq_pushstring( v, _SC("sfVideoMode"), -1 ); sq_get( v, -2 );
	sq_push( v, -2 ); // env
	sq_pushinteger( v, dtvm.width ); sq_pushinteger( v, dtvm.height );
	sq_pushinteger( v, dtvm.bitsPerPixel );
	sq_call( v, 4, SQTrue, SQTrue );

	return 1;
}

// ==========
// sfKeyboard
static SQInteger Keyboard_isKeyPressed( HSQUIRRELVM v ) {
	SQInteger k = -1;
	sq_getinteger( v, 2, &k );
	sq_pushbool( v, sf::Keyboard::isKeyPressed( (sf::Keyboard::Key)k ) );
	return 1;
}

// =======
// sfMouse
static SQInteger Mouse_getPosition( HSQUIRRELVM v ) {
	sf::Vector2i mpos;

	if ( sq_gettop( v ) > 1 ) {
		if ( sq_isinstanceof( v, 2, _SC("sfWindow") ) ) {
			Window* win = NULL;
			sq_getinstanceup( v, 2, (SQUserPointer*)&win, 0 );
			mpos = sf::Mouse::getPosition( *win->rwin );
		} else {
			return sq_throwerror( v, _SC("Parameter 1 is not a valid sfWindow instance.") );
		}
	} else {
		mpos = sf::Mouse::getPosition();
	}

	sqsf_pushvector( v, sf::Vector2f( mpos.x, mpos.y ) );
	return 1;
}

static SQInteger Mouse_isButtonPressed( HSQUIRRELVM v ) {
	SQInteger mbi = 0;
	sq_getinteger( v, 2, &mbi );
	SQBool bres = sf::Mouse::isButtonPressed( (sf::Mouse::Button)mbi );
	sq_pushbool( v, bres );
	return 1;
}

void Window_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfWindow"), -1 );
	sq_pushstring( v, _SC("sfRenderTarget"), -1 ); sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	sq_register_functions( v, Window_RegFunctions );
	sq_newslot( v, -3, SQFalse );

	// static sfVideoMode members
	sq_pushstring( v, _SC("sfVideoMode"), -1 ); sq_get( v, -2 );
	sq_pushstring( v, _SC("getDesktopMode"), -1 );
	sq_newclosure( v, VideoMode_getDesktopMode, 0 );
	sq_setparamscheck( v, 1, NULL );
	sq_setnativeclosurename( v, -1, _SC("getDesktopMode") );
	sq_newslot( v, -3, SQTrue );
	sq_pop( v, 1 );

	// static sfKeyboard members
	sq_pushstring( v, _SC("sfKeyboard"), -1 );
	sq_newtable( v );
	sq_pushstring( v, _SC("isKeyPressed"), -1 );
	sq_newclosure( v, Keyboard_isKeyPressed, 0 );
	sq_setparamscheck( v, 2, _SC(".i") );
	sq_setnativeclosurename( v, -1, _SC("isKeyPressed") );
	sq_newslot( v, -3, SQTrue );
	sq_newslot( v, -3, SQFalse );

	// static sfMouse members
	sq_pushstring( v, _SC("sfMouse"), -1 );
	sq_newtable( v );
	sq_pushstring( v, _SC("getPosition"), -1 );
	sq_newclosure( v, Mouse_getPosition, 0 );
	sq_setparamscheck( v, -1, _SC(".x") );
	sq_setnativeclosurename( v, -1, _SC("getPosition") );
	sq_newslot( v, -3, SQTrue );
	sq_pushstring( v, _SC("isButtonPressed"), -1 );
	sq_newclosure( v, Mouse_isButtonPressed, 0 );
	sq_setparamscheck( v, 2, _SC(".i") );
	sq_setnativeclosurename( v, -1, _SC("isButtonPressed") );
	sq_newslot( v, -3, SQTrue );
	sq_newslot( v, -3, SQFalse );
}

}
