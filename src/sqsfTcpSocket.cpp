#include "sqsfGlobal.h"
#include "sqsfTcpSocket.h"

namespace sqsf {

static SQInteger TcpSocket_release( SQUserPointer p, SQInteger size ) {
	sf::TcpSocket* stcp = (sf::TcpSocket*)p; (void)size;
	delete stcp;
	return 1;
}

static SQInteger TcpSocket_construct( HSQUIRRELVM v ) {
	sf::TcpSocket* stcp = new sf::TcpSocket();
	sq_setinstanceup( v, 1, stcp );
	sq_setreleasehook( v, 1, TcpSocket_release );
	return 0;
}

static SQInteger TcpSocket_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfTcpSocket"), -1 );
	return 1;
}

#define GET_SELF() sf::TcpSocket* self = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&self, 0 )

static SQInteger TcpSocket_setBlocking( HSQUIRRELVM v ) {
	GET_SELF();
	SQBool bBlock = true;
	sq_getbool( v, 2, &bBlock );
	self->setBlocking( bBlock );
	return 0;
}

static SQInteger TcpSocket_connect( HSQUIRRELVM v ) {
	GET_SELF();
	const SQChar* addstr;
	SQInteger port = 0;
	SQFloat tosec = 0.0f;

	sq_getstring( v, 2, &addstr );
	sq_getinteger( v, 3, &port );
	if ( sq_gettop( v ) > 3 ) {
		sq_getfloat( v, 4, &tosec );
	}

	sf::Socket::Status stat = self->connect(
		sf::IpAddress( addstr ), (unsigned short)port, sf::seconds( tosec ) );

	sq_pushinteger( v, stat );

	return 1;
}

static SQInteger TcpSocket_receive( HSQUIRRELVM v ) {
	GET_SELF();

	std::size_t dsz = 0;
	sq_getinteger( v, 3, (SQInteger*)&dsz );
	char data[dsz];
	std::size_t rcsz = 0;

	sf::Socket::Status stat = self->receive( data, dsz, rcsz );

	sq_pushstring( v, _SC("data"), -1 );
	sq_pushstring( v, data, rcsz );
	sq_newslot( v, 2, SQFalse );
	sq_pushstring( v, _SC("size"), -1 );
	sq_pushinteger( v, rcsz );
	sq_newslot( v, 2, SQFalse );

	/*data[rcsz] = '\0';
	scprintf( "Received: %u - %s\n", rcsz, data );*/

	sq_pushinteger( v, stat );
	return 1;
}

#undef GET_SELF

static SQRegFunction TcpSocket_RegFunctions[] = {
	{_SC("constructor"), TcpSocket_construct, 1, _SC(".")},
	{_SC("_typeof"), TcpSocket_typeof, 1, _SC("x")},
	{_SC("setBlocking"), TcpSocket_setBlocking, 2, _SC("xb")},
	{_SC("connect"), TcpSocket_connect, -3, _SC("xsin")},
	{_SC("receive"), TcpSocket_receive, 3, _SC("xti")},
	{0, 0, 0, 0}
};

void TcpSocket_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfTcpSocket"), -1 );
	sq_newclass( v, SQFalse );
	sq_register_functions( v, TcpSocket_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

};
