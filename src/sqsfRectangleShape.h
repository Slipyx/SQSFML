#ifndef __SQSFRECTANGLESHAPE_H
#define __SQSFRECTANGLESHAPE_H

//#define SQSF_TFA_TYPE_TAG 0x10000000

#include <squirrel.h>
#include "sqsfShape.h"
#include <SFML/Graphics/RectangleShape.hpp>

namespace sqsf {

struct RectangleShape : Shape {
	sf::RectangleShape* rshape;
};

void RectangleShape_register( HSQUIRRELVM v );

}

#endif
