#include "sqsfGlobal.h"
#include "sqsfRenderTarget.h"
#include "sqsfTransformable.h"

namespace sqsf {

static SQInteger RenderTarget_construct( HSQUIRRELVM v ) {
	return sq_throwerror( v, _SC("sfRenderTarget is an abstract base class") );
}

static SQInteger RenderTarget_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfRenderTarget"), -1 );
	return 1;
}

#define GET_SELF() RenderTarget* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::RenderTarget* self = _s->rtar

static SQInteger RenderTarget_clear( HSQUIRRELVM v ) {
	sf::Color col;
	GET_SELF();

	if ( sq_gettop( v ) > 1 ) {
		VALIDATE_INSTANCEOF( v, 2, _SC("sfColor") );
		sqsf_getcolor( v, 2, col );
	}

	self->clear( col );
	return 0;
}

static SQInteger RenderTarget_draw( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfDrawable") );

	sf::RenderStates rstates;
	if ( sq_gettop( v ) > 2 ) {
		if ( sq_isinstanceof( v, 3, _SC("sfTexture") ) ) {
			sf::Texture* tex = NULL;
			sq_getinstanceup( v, 3, (SQUserPointer*)&tex, 0 );
			rstates.texture = tex;
		} else {
			return sq_throwerror( v, _SC("Parameter 2 is not a valid instance.") );
		}
	}

	Transformable* stfa = NULL;
	sq_getinstanceup( v, 2, (SQUserPointer*)&stfa, 0 );
	self->draw( *(stfa->dra), rstates );

	return 0;
}

#undef GET_SELF

static SQRegFunction RenderTarget_RegFunctions[] = {
	{_SC("constructor"), RenderTarget_construct, 1, _SC(".")},
	{_SC("_typeof"), RenderTarget_typeof, 1, _SC("x")},
	{_SC("clear"), RenderTarget_clear, -1, _SC("xx")},
	{_SC("draw"), RenderTarget_draw, -2, _SC("xxx")},
	{0, 0, 0, 0}
};

void RenderTarget_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfRenderTarget"), -1 );
	sq_newclass( v, SQFalse );
	sq_register_functions( v, RenderTarget_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
