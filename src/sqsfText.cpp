#include "sqsfGlobal.h"
#include "sqsfText.h"

//#define SQSF_TEXT_TYPE_TAG (SQSF_TFA_TYPE_TAG | 0x00000004)

namespace sqsf {

static SQInteger Text_release( SQUserPointer p, SQInteger size ) {
	Text* stxt = (Text*)p; (void)size;
	delete stxt->txt;
	sq_free( stxt, sizeof (Text) );
	return 1;
}

static SQInteger Text_construct( HSQUIRRELVM v ) {
	const SQChar* txtstr = NULL;
	sf::Font* txtfnt = NULL;
	SQInteger sz = 30;
	VALIDATE_INSTANCEOF( v, 3, _SC("sfFont") );

	sq_getstring( v, 2, &txtstr );
	sq_getinstanceup( v, 3, (SQUserPointer*)&txtfnt, 0 );

	if ( sq_gettop( v ) > 3 )
		sq_getinteger( v, 4, &sz );

	Text* stxt = (Text*)sq_malloc( sizeof (Text) );
	stxt->txt = new sf::Text( txtstr, *txtfnt, sz );
	stxt->tfa = (sf::Transformable*)stxt->txt;
	stxt->dra = (sf::Drawable*)stxt->txt;

	sq_setinstanceup( v, 1, stxt );
	sq_setreleasehook( v, 1, Text_release );

	return 0;
}

static SQInteger Text_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfText"), -1 );
	return 1;
}

#define GET_SELF() Text* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::Text* self = _s->txt //(SQUserPointer)SQSF_TEXT_TYPE_TAG )

static SQInteger Text_setString( HSQUIRRELVM v ) {
	const SQChar* txtstr = NULL;
	GET_SELF();

	sq_getstring( v, 2, &txtstr );
	self->setString( txtstr );

	return 0;
}

static SQInteger Text_getString( HSQUIRRELVM v ) {
	GET_SELF();

	sq_pushstring( v, std::string( self->getString() ).c_str(), -1 );

	return 1;
}

static SQInteger Text_getLocalBounds( HSQUIRRELVM v ) {
	GET_SELF();

	sqsf_pushrect( v, self->getLocalBounds() );

	return 1;
}

static SQInteger Text_getGlobalBounds( HSQUIRRELVM v ) {
	GET_SELF();

	sqsf_pushrect( v, self->getGlobalBounds() );

	return 1;
}

static SQInteger Text_setColor( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfColor") );
	sf::Color nc;
	sqsf_getcolor( v, 2, nc );
	self->setColor( nc );
	return 0;
}

#undef GET_SELF

static SQRegFunction Text_RegFunctions[] = {
	{_SC("constructor"), Text_construct, -3, _SC(".sxi")},
	{_SC("_typeof"), Text_typeof, 1, _SC("x")},
	{_SC("setString"), Text_setString, 2, _SC("xs")},
	{_SC("getString"), Text_getString, 1, _SC("x")},
	{_SC("getLocalBounds"), Text_getLocalBounds, 1, _SC("x")},
	{_SC("getGlobalBounds"), Text_getGlobalBounds, 1, _SC("x")},
	{_SC("setColor"), Text_setColor, 2, _SC("xx")},
	{0, 0, 0, 0}
};

void Text_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfText"), -1 );
	sq_pushstring( v, _SC("sfDrawable"), -1 );
	sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	//sq_settypetag( v, -1, (SQUserPointer)SQSF_TEXT_TYPE_TAG );
	sq_register_functions( v, Text_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
