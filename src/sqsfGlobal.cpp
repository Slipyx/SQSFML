//#include <cstring>

#include "sqsfGlobal.h"

void sqsf_getvector( HSQUIRRELVM v, SQInteger ix, sf::Vector2f& vec ) {
	if ( ix < 0 ) ix = sq_gettop( v ) + (ix+1);
	sq_pushstring( v, _SC("x"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("y"), -1 ); sq_get( v, ix );
	sq_getfloat( v, -2, &vec.x ); sq_getfloat( v, -1, &vec.y );
	sq_pop( v, 2 );
}

void sqsf_pushvector( HSQUIRRELVM v, const sf::Vector2f& vec ) {
	sq_pushroottable( v );
	sq_pushstring( v, _SC("sfVec2"), -1 ); sq_get( v, -2 );
	sq_push( v, -2 ); // env
	sq_pushfloat( v, vec.x ); sq_pushfloat( v, vec.y );
	sq_call( v, 3, SQTrue, SQTrue );
	sq_remove( v, -2 ); sq_remove( v, -2 );
}

void sqsf_getcolor( HSQUIRRELVM v, SQInteger ix, sf::Color& c ) {
	if ( ix < 0 ) ix = sq_gettop( v ) + (ix+1);
	SQInteger cr, cg, cb, ca;
	sq_pushstring( v, _SC("r"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("g"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("b"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("a"), -1 ); sq_get( v, ix );
	sq_getinteger( v, -4, &cr ); sq_getinteger( v, -3, &cg );
	sq_getinteger( v, -2, &cb ); sq_getinteger( v, -1, &ca );
	sq_pop( v, 4 );
	c.r = cr; c.g = cg; c.b = cb; c.a = ca;
}

void sqsf_pushcolor( HSQUIRRELVM v, const sf::Color& c ) {
	sq_pushroottable( v );
	sq_pushstring( v, _SC("sfColor"), -1 ); sq_get( v, -2 );
	sq_push( v, -2 ); // env
	sq_pushinteger( v, c.r ); sq_pushinteger( v, c.g ); sq_pushinteger( v, c.b );
	sq_pushinteger( v, c.a );
	sq_call( v, 5, SQTrue, SQTrue );
	sq_remove( v, -2 ); sq_remove( v, -2 );
}

void sqsf_getvertex( HSQUIRRELVM v, SQInteger ix, sf::Vertex& vt ) {
	if ( ix < 0 ) ix = sq_gettop( v ) + (ix+1);
	sf::Vector2f vtpos, vttc; sf::Color vtcol;
	sq_pushstring( v, _SC("position"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("color"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("texCoords"), -1 ); sq_get( v, ix );
	sqsf_getvector( v, -3, vtpos ); sqsf_getcolor( v, -2, vtcol ); sqsf_getvector( v, -1, vttc );
	sq_pop( v, 3 );
	vt.position = vtpos; vt.color = vtcol; vt.texCoords = vttc;
}

void sqsf_pushvertex( HSQUIRRELVM v, const sf::Vertex& vt ) {
	sq_pushroottable( v );
	sq_pushstring( v, _SC("sfVertex"), -1 ); sq_get( v, -2 );
	sq_push( v, -2 ); // env
	sqsf_pushvector( v, vt.position ); sqsf_pushcolor( v, vt.color ); sqsf_pushvector( v, vt.texCoords );
	sq_call( v, 4, SQTrue, SQTrue );
	sq_remove( v, -2 ); sq_remove( v, -2 );
}

void sqsf_getrect( HSQUIRRELVM v, SQInteger ix, sf::IntRect& rect ) {
	if ( ix < 0 ) ix = sq_gettop( v ) + (ix+1);
	sq_pushstring( v, _SC("left"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("top"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("width"), -1 ); sq_get( v, ix );
	sq_pushstring( v, _SC("height"), -1 ); sq_get( v, ix );
	sq_getinteger( v, -4, &rect.left ); sq_getinteger( v, -3, &rect.top );
	sq_getinteger( v, -2, &rect.width ); sq_getinteger( v, -1, &rect.height );
	sq_pop( v, 4 );
}

void sqsf_pushrect( HSQUIRRELVM v, const sf::FloatRect& rect ) {
	sq_pushroottable( v );
	sq_pushstring( v, _SC("sfRect"), -1 ); sq_get( v, -2 );
	sq_push( v, -2 ); // env
	sq_pushfloat( v, rect.left ); sq_pushfloat( v, rect.top );
	sq_pushfloat( v, rect.width ); sq_pushfloat( v, rect.height );
	sq_call( v, 5, SQTrue, SQTrue );
	sq_remove( v, -2 ); sq_remove( v, -2 );
}

/*
static void sqsfGlobal_register( HSQUIRRELVM v ) {
	if ( SQ_SUCCEEDED( sq_compilebuffer( v, gScriptSrc, scstrlen( gScriptSrc ), _SC("Global Script"), SQTrue ) ) ) {
		sq_pushroottable( v );
		sq_call( v, 1, SQFalse, SQTrue );
		sq_pop( v, 1 );
	}
}
*/
