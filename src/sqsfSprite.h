#ifndef __SQSFSPRITE_H
#define __SQSFSPRITE_H

#include <squirrel.h>
#include "sqsfTransformable.h"
#include <SFML/Graphics/Sprite.hpp>

namespace sqsf {

struct Sprite : Transformable {
	sf::Sprite* spr;
};

void Sprite_register( HSQUIRRELVM v );

}

#endif
