#include "sqsfGlobal.h"
#include "sqsfCircleShape.h"

namespace sqsf {

static SQInteger CircleShape_release( SQUserPointer p, SQInteger size ) {
	CircleShape* scshape = (CircleShape*)p; (void)size;
	delete scshape->cshape;
	sq_free( scshape, sizeof (CircleShape) );
	return 1;
}

static SQInteger CircleShape_construct( HSQUIRRELVM v ) {
	CircleShape* scshape = (CircleShape*)sq_malloc( sizeof (CircleShape) );
	float rd = 0.0f;
	size_t pc = 30;

	if ( sq_gettop( v ) > 1 ) {
		sq_getfloat( v, 2, &rd );
		if ( sq_gettop( v ) > 2 ) {
			sq_getinteger( v, 3, (SQInteger*)&pc );
		}
	}

	scshape->cshape = new sf::CircleShape( rd, pc );
	scshape->tfa = (sf::Transformable*)scshape->cshape;
	scshape->dra = (sf::Drawable*)scshape->cshape;
	scshape->shape = (sf::Shape*)scshape->cshape;

	sq_setinstanceup( v, 1, scshape );
	sq_setreleasehook( v, 1, CircleShape_release );
	return 0;
}

static SQInteger CircleShape_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfCircleShape"), -1 );
	return 1;
}

#define GET_SELF() CircleShape* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::CircleShape* self = _s->cshape

static SQInteger CircleShape_setRadius( HSQUIRRELVM v ) {
	GET_SELF();
	float nrd = 0;
	sq_getfloat( v, 2, &nrd );
	self->setRadius( nrd );
	return 0;
}

static SQInteger CircleShape_getRadius( HSQUIRRELVM v ) {
	GET_SELF();
	sq_pushfloat( v, self->getRadius() );
	return 1;
}

#undef GET_SELF

static SQRegFunction CircleShape_RegFunctions[] = {
	{_SC("constructor"), CircleShape_construct, -1, _SC(".ni")},
	{_SC("_typeof"), CircleShape_typeof, 1, _SC("x")},
	{_SC("setRadius"), CircleShape_setRadius, 2, _SC("xn")},
	{_SC("getRadius"), CircleShape_getRadius, 1, _SC("x")},
	{0, 0, 0, 0}
};

void CircleShape_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfCircleShape"), -1 );
	sq_pushstring( v, _SC("sfShape"), -1 );
	sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	sq_register_functions( v, CircleShape_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
