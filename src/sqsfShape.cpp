#include "sqsfGlobal.h"
#include "sqsfShape.h"

namespace sqsf {

/*static SQInteger Shape_release( SQUserPointer p, SQInteger size ) {
	sf::Shape* shape = (sf::Shape*)p; (void)size;
	delete shape;
	return 1;
}*/

static SQInteger Shape_construct( HSQUIRRELVM v ) {
	//sf::Shape* shape = new sf::Shape();
	return sq_throwerror( v, _SC("sfShape is an abstract base class") );
	//sq_setinstanceup( v, 1, shape );
	//sq_setreleasehook( v, 1, Shape_release );
	//return 0;
}

static SQInteger Shape_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfShape"), -1 );
	return 1;
}

#define GET_SELF() Shape* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::Shape* self = _s->shape

static SQInteger Shape_setTexture( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfTexture") );
	sf::Texture* tex = NULL;
	SQBool breset = SQFalse;
	sq_getinstanceup( v, 2, (SQUserPointer*)&tex, 0 );
	if ( sq_gettop( v ) > 2 ) {
		sq_getbool( v, 3, &breset );
	}
	self->setTexture( tex, breset );
	return 0;
}

static SQInteger Shape_setFillColor( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfColor") );
	sf::Color col;
	sqsf_getcolor( v, 2, col );
	self->setFillColor( col );
	return 0;
}

static SQInteger Shape_setOutlineThickness( HSQUIRRELVM v ) {
	GET_SELF();
	SQFloat nthick;
	sq_getfloat( v, 2, &nthick );
	self->setOutlineThickness( nthick );
	return 0;
}

static SQInteger Shape_setOutlineColor( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfColor") );
	sf::Color ncol;
	sqsf_getcolor( v, 2, ncol );
	self->setOutlineColor( ncol);
	return 0;
}

#undef GET_SELF

static SQRegFunction Shape_RegFunctions[] = {
	{_SC("constructor"), Shape_construct, 1, _SC(".")},
	{_SC("_typeof"), Shape_typeof, 1, _SC("x")},
	{_SC("setTexture"), Shape_setTexture, -2, _SC("xxb")},
	{_SC("setFillColor"), Shape_setFillColor, 2, _SC("xx")},
	{_SC("setOutlineThickness"), Shape_setOutlineThickness, 2, _SC("xn")},
	{_SC("setOutlineColor"), Shape_setOutlineColor, 2, _SC("xx")},
	{0, 0, 0, 0}
};

void Shape_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfShape"), -1 );
	sq_pushstring( v, _SC("sfDrawable"), -1 );
	sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	sq_register_functions( v, Shape_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
