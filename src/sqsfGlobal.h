#ifndef __SQSFGLOBAL_H
#define __SQSFGLOBAL_H

//#include <cstring>

#include "squtil.h"
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

void sqsf_getvector( HSQUIRRELVM v, SQInteger ix, sf::Vector2f& vec );
void sqsf_pushvector( HSQUIRRELVM v, const sf::Vector2f& vec );

void sqsf_getcolor( HSQUIRRELVM v, SQInteger ix, sf::Color& c );
void sqsf_pushcolor( HSQUIRRELVM v, const sf::Color& c );

void sqsf_getvertex( HSQUIRRELVM v, SQInteger ix, sf::Vertex& vt );
void sqsf_pushvertex( HSQUIRRELVM v, const sf::Vertex& vt );

void sqsf_getrect( HSQUIRRELVM v, SQInteger ix, sf::IntRect& rect );
void sqsf_pushrect( HSQUIRRELVM v, const sf::FloatRect& rect );

/*
static void sqsfGlobal_register( HSQUIRRELVM v ) {
	if ( SQ_SUCCEEDED( sq_compilebuffer( v, gScriptSrc, scstrlen( gScriptSrc ), _SC("Global Script"), SQTrue ) ) ) {
		sq_pushroottable( v );
		sq_call( v, 1, SQFalse, SQTrue );
		sq_pop( v, 1 );
	}
}
*/
#endif
