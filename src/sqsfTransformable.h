#ifndef __SQSFTRANSFORMABLE_H
#define __SQSFTRANSFORMABLE_H

//#define SQSF_TFA_TYPE_TAG 0x10000000

#include <squirrel.h>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>

namespace sqsf {

struct Transformable {
	sf::Transformable* tfa;
	sf::Drawable* dra;
};

void Transformable_register( HSQUIRRELVM v );

}

#endif
