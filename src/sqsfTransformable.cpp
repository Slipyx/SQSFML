//#define SQSF_TFA_TYPE_TAG 0x10000000

#include "sqsfGlobal.h"
#include "sqsfTransformable.h"

namespace sqsf {

static SQInteger Transformable_release( SQUserPointer p, SQInteger size ) {
	Transformable* stfa = (Transformable*)p; (void)size;
	delete stfa->tfa;
	sq_free( stfa, sizeof (Transformable) );
	return 1;
}

static SQInteger Transformable_construct( HSQUIRRELVM v ) {
	Transformable* stfa = (Transformable*)sq_malloc( sizeof (Transformable) );
	stfa->tfa = new sf::Transformable();
	sq_setinstanceup( v, 1, stfa );
	sq_setreleasehook( v, 1, Transformable_release );
	return 0;
}

static SQInteger Transformable_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfTransformable"), -1 );
	return 1;
}

#define GET_SELF() Transformable* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	if ( _s->tfa == NULL ) return 0; sf::Transformable* self = _s->tfa

static SQInteger Transformable_setPosition( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );

	sf::Vector2f npos;
	sqsf_getvector( v, 2, npos );

	self->setPosition( npos );

	return 0;
}

static SQInteger Transformable_getPosition( HSQUIRRELVM v ) {
	GET_SELF();

	sf::Vector2f spos = self->getPosition();
	sq_pushroottable( v );
	sq_pushstring( v, _SC("sfVec2"), -1 );
	sq_get( v, -2 );
	sq_pushroottable( v ); // env
	sq_pushfloat( v, spos.x ); sq_pushfloat( v, spos.y );
	sq_call( v, 3, SQTrue, SQTrue );

	return 1;
}

static SQInteger Transformable_setRotation( HSQUIRRELVM v ) {
	SQFloat nrot = 0.0f;
	GET_SELF();

	sq_getfloat( v, 2, &nrot );
	self->setRotation( nrot );

	return 0;
}

static SQInteger Transformable_getRotation( HSQUIRRELVM v ) {
	SQFloat srot = 0.0f;
	GET_SELF();

	srot = self->getRotation();
	sq_pushfloat( v, srot );

	return 1;
}

static SQInteger Transformable_setScale( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );
	sf::Vector2f nsc;
	sqsf_getvector( v, 2, nsc );
	self->setScale( nsc );
	return 0;
}

static SQInteger Transformable_setOrigin( HSQUIRRELVM v ) {
	GET_SELF();

	VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );
	sq_pushstring( v, _SC("x"), -1 ); sq_get( v, 2 );
	sq_pushstring( v, _SC("y"), -1 ); sq_get( v, 2 );

	SQFloat norx, nory;
	sq_getfloat( v, -2, &norx ); sq_getfloat( v, -1, &nory );
	sq_pop( v, 2 );

	self->setOrigin( sf::Vector2f( norx, nory ) );

	return 0;
}

static SQInteger Transformable_move( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );

	sf::Vector2f mvec;
	sqsf_getvector( v, 2, mvec );
	self->move( mvec );

	return 0;
}

#undef GET_SELF

static SQRegFunction Transformable_RegFunctions[] = {
	{_SC("constructor"), Transformable_construct, 1, _SC(".")},
	{_SC("_typeof"), Transformable_typeof, 1, _SC("x")},
	{_SC("setPosition"), Transformable_setPosition, 2, _SC("xx")},
	{_SC("getPosition"), Transformable_getPosition, 1, _SC("x")},
	{_SC("setRotation"), Transformable_setRotation, 2, _SC("xn")},
	{_SC("getRotation"), Transformable_getRotation, 1, _SC("x")},
	{_SC("setScale"), Transformable_setScale, 2, _SC("xx")},
	{_SC("setOrigin"), Transformable_setOrigin, 2, _SC("xx")},
	{_SC("move"), Transformable_move, 2, _SC("xx")},
	{0, 0, 0, 0}
};

void Transformable_register( HSQUIRRELVM v ) {
	// transformable class
	sq_pushstring( v, _SC("sfTransformable"), -1 );
	sq_newclass( v, SQFalse );
	//sq_settypetag( v, -1, (SQUserPointer)SQSF_TFA_TYPE_TAG );
	sq_register_functions( v, Transformable_RegFunctions );
	sq_newslot( v, -3, SQFalse );

	// abstract drawable class
	sq_pushstring( v, _SC("sfDrawable"), -1 );
	sq_pushstring( v, _SC("sfTransformable"), -1 );
	sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	// TODO: register abstract draw function?
	sq_newslot( v, -3, SQFalse );
}

}
