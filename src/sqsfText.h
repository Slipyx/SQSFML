#ifndef __SQSFTEXT_H
#define __SQSFTEXT_H

#include <squirrel.h>
#include "sqsfTransformable.h"
#include <SFML/Graphics/Text.hpp>

namespace sqsf {

struct Text : Transformable {
	sf::Text* txt;
};

void Text_register( HSQUIRRELVM v );

}

#endif
