#include "sqsfGlobal.h"
#include "sqsfTexture.h"

namespace sqsf {

static SQInteger Texture_release( SQUserPointer p, SQInteger size ) {
	sf::Texture* tex = (sf::Texture*)p; (void)size;
	delete tex;
	return 1;
}

static SQInteger Texture_construct( HSQUIRRELVM v ) {
	sf::Texture* tex = new sf::Texture();

	if ( sq_gettop( v ) > 1 ) {
		const SQChar* s;
		sq_getstring( v, 2, &s );
		tex->loadFromFile( std::string( s ) );
	}

	sq_setinstanceup( v, 1, tex );
	sq_setreleasehook( v, 1, Texture_release );
	return 0;
}

static SQInteger Texture_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfTexture"), -1 );
	return 1;
}

#define GET_SELF() sf::Texture* self = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&self, 0 )

static SQInteger Texture_getSize( HSQUIRRELVM v ) {
	GET_SELF();

	sf::Vector2u tsz = self->getSize();
	sqsf_pushvector( v, sf::Vector2f( tsz.x, tsz.y ) );

	return 1;
}

static SQInteger Texture_setSmooth( HSQUIRRELVM v ) {
	GET_SELF();
	SQBool bsmooth = SQFalse;
	sq_getbool( v, 2, &bsmooth );
	self->setSmooth( bsmooth );
	return 0;
}

static SQInteger Texture_setRepeated( HSQUIRRELVM v ) {
	GET_SELF();
	SQBool brep = SQFalse;
	sq_getbool( v, 2, &brep );
	self->setRepeated( brep );
	return 0;
}

#undef GET_SELF

static SQRegFunction Texture_RegFunctions[] = {
	{_SC("constructor"), Texture_construct, -1, _SC(".s")},
	{_SC("_typeof"), Texture_typeof, 1, _SC("x")},
	{_SC("getSize"), Texture_getSize, 1, _SC("x")},
	{_SC("setSmooth"), Texture_setSmooth, 2, _SC("xb")},
	{_SC("setRepeated"), Texture_setRepeated, 2, _SC("xb")},
	{0, 0, 0, 0}
};

void Texture_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfTexture"), -1 );
	sq_newclass( v, SQFalse );
	sq_register_functions( v, Texture_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
