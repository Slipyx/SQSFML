#include "sqsfGlobal.h"
#include "sqsfRenderTexture.h"

namespace sqsf {

static SQInteger RenderTexture_release( SQUserPointer p, SQInteger size ) {
	RenderTexture* srtex = (RenderTexture*)p; (void)size;
	delete srtex->rtex;
	sq_free( srtex, sizeof (RenderTexture) );
	return 1;
}

static SQInteger RenderTexture_construct( HSQUIRRELVM v ) {
	SQInteger rtw, rth;
	SQBool bdb = SQFalse;
	RenderTexture* srtex = (RenderTexture*)sq_malloc( sizeof (RenderTexture) );

	sq_getinteger( v, 2, &rtw );
	sq_getinteger( v, 3, &rth );
	if ( sq_gettop( v ) > 3 ) {
		sq_getbool( v, 4, &bdb );
	}

	srtex->rtex = new sf::RenderTexture;
	srtex->rtex->create( rtw, rth, bdb );
	srtex->rtar = (sf::RenderTarget*)srtex->rtex;

	sq_setinstanceup( v, 1, srtex );
	sq_setreleasehook( v, 1, RenderTexture_release );
	return 0;
}

static SQInteger RenderTexture_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfRenderTexture"), -1 );
	return 1;
}

#define GET_SELF() RenderTexture* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::RenderTexture* self = _s->rtex

static SQInteger RenderTexture_display( HSQUIRRELVM v ) {
	GET_SELF();
	self->display();
	return 0;
}

static SQInteger RenderTexture_setSmooth( HSQUIRRELVM v ) {
	GET_SELF();
	SQBool bsmoo = SQFalse;
	sq_getbool( v, 2, &bsmoo );
	self->setSmooth( bsmoo );
	return 0;
}

static SQInteger RenderTexture_getSize( HSQUIRRELVM v ) {
	GET_SELF();
	sf::Vector2u sz = self->getSize();
	sqsf_pushvector( v, sf::Vector2f( sz.x, sz.y ) );
	return 1;
}

/*static SQInteger RenderTexture_getTexture( HSQUIRRELVM v ) {
	GET_SELF();
	const sf::Texture& srttex = self->getTexture();
	sq_pushinteger( v, (SQInteger)&srttex );
	return 1;
}*/

#undef GET_SELF

static SQRegFunction RenderTexture_RegFunctions[] = {
	{_SC("constructor"), RenderTexture_construct, -3, _SC(".iib")},
	{_SC("_typeof"), RenderTexture_typeof, 1, _SC("x")},
	{_SC("display"), RenderTexture_display, 1, _SC("x")},
	{_SC("setSmooth"), RenderTexture_setSmooth, 2, _SC("xb")},
	{_SC("getSize"), RenderTexture_getSize, 1, _SC("x")},
	//{_SC("getTexture"), RenderTexture_getTexture, 1, _SC("x")},
	{0, 0, 0, 0}
};

void RenderTexture_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfRenderTexture"), -1 );
	sq_pushstring( v, _SC("sfRenderTarget"), -1 ); sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	sq_register_functions( v, RenderTexture_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
