//#include <cstdio>
#include <cstdarg>
//#include <iostream>

#include "sqrand.h"
#include "sqsfGlobal.h"
#include "sqsfClock.h"
#include "sqsfTexture.h"
#include "sqsfRenderTarget.h"
#include "sqsfWindow.h"
#include "sqsfRenderTexture.h"
#include "sqsfTransformable.h"
#include "sqsfVertexArray.h"
#include "sqsfFont.h"
#include "sqsfText.h"
#include "sqsfShape.h"
#include "sqsfRectangleShape.h"
#include "sqsfCircleShape.h"
#include "sqsfSprite.h"
#include "sqsfTcpSocket.h"

//#include <squirrel.h>
#include <sqstdblob.h>
#include <sqstdio.h>
#include <sqstdsystem.h>
#include <sqstdmath.h>
#include <sqstdstring.h>
#include <sqstdaux.h>

#ifdef SQUNICODE
#define scvprintf vfwprintf
#else
#define scvprintf vfprintf
#endif

static void sq_printfunc( HSQUIRRELVM v, const SQChar* s, ... ) {
	va_list vl;
	va_start( vl, s );
	scvprintf( stdout, s, vl );
	va_end( vl );
	(void)v;
}

HSQUIRRELVM sqVM = NULL;

int main( int argc, char** argv ) {
	sqVM = sq_open( 1024 );
	sq_setprintfunc( sqVM, sq_printfunc, sq_printfunc );

	sq_pushroottable( sqVM );

	sqstd_register_bloblib( sqVM );
	sqstd_register_iolib( sqVM );
	sqstd_register_systemlib( sqVM );
	sqstd_register_mathlib( sqVM );
	sqstd_register_stringlib( sqVM );
	sqstd_seterrorhandlers( sqVM );

	// ICE TEST
	/*IceKey icek; // thin
	std::string thekey = "zWnjPb\\>";
	icek.set( (const unsigned char*)thekey.c_str() );*/

	sqrand_register( sqVM );

	//sqsfGlobal_register( sqVM );
	sqstd_dofile( sqVM, _SC("global.nut"), SQFalse, SQTrue );
	sqsf::Clock_register( sqVM );
	sqsf::Texture_register( sqVM );
	sqsf::RenderTarget_register( sqVM );
	sqsf::Window_register( sqVM );
	sqsf::RenderTexture_register( sqVM );
	sqsf::Transformable_register( sqVM );
	sqsf::VertexArray_register( sqVM );
	sqsf::Font_register( sqVM );
	sqsf::Text_register( sqVM );
	sqsf::Shape_register( sqVM );
	sqsf::RectangleShape_register( sqVM );
	sqsf::CircleShape_register( sqVM );
	sqsf::Sprite_register( sqVM );
	sqsf::TcpSocket_register( sqVM );

	sqstd_dofile( sqVM, _SC("main.nut"), SQFalse, SQTrue );

	sq_settop( sqVM, 0 ); // clear the stack
	sq_close( sqVM );

	(void)argc; (void)argv;
	return EXIT_SUCCESS;
}
