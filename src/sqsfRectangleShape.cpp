#include "sqsfGlobal.h"
#include "sqsfRectangleShape.h"

namespace sqsf {

static SQInteger RectangleShape_release( SQUserPointer p, SQInteger size ) {
	RectangleShape* srshape = (RectangleShape*)p; (void)size;
	delete srshape->rshape;
	sq_free( srshape, sizeof (RectangleShape) );
	return 1;
}

static SQInteger RectangleShape_construct( HSQUIRRELVM v ) {
	RectangleShape* srshape = (RectangleShape*)sq_malloc( sizeof (RectangleShape) );
	sf::Vector2f sz;

	if ( sq_gettop( v ) > 1 ) {
		VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );
		sqsf_getvector( v, 2, sz );
	}

	srshape->rshape = new sf::RectangleShape( sz );
	srshape->tfa = (sf::Transformable*)srshape->rshape;
	srshape->dra = (sf::Drawable*)srshape->rshape;
	srshape->shape = (sf::Shape*)srshape->rshape;

	sq_setinstanceup( v, 1, srshape );
	sq_setreleasehook( v, 1, RectangleShape_release );
	return 0;
}

static SQInteger RectangleShape_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfRectangleShape"), -1 );
	return 1;
}

#define GET_SELF() RectangleShape* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::RectangleShape* self = _s->rshape

static SQInteger RectangleShape_setSize( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfVec2") );
	sf::Vector2f nsz;
	sqsf_getvector( v, 2, nsz );
	self->setSize( nsz );
	return 0;
}

static SQInteger RectangleShape_getSize( HSQUIRRELVM v ) {
	GET_SELF();
	sqsf_pushvector( v, self->getSize() );
	return 1;
}

#undef GET_SELF

static SQRegFunction RectangleShape_RegFunctions[] = {
	{_SC("constructor"), RectangleShape_construct, -1, _SC(".x")},
	{_SC("_typeof"), RectangleShape_typeof, 1, _SC("x")},
	{_SC("setSize"), RectangleShape_setSize, 2, _SC("xx")},
	{_SC("getSize"), RectangleShape_getSize, 1, _SC("x")},
	{0, 0, 0, 0}
};

void RectangleShape_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfRectangleShape"), -1 );
	sq_pushstring( v, _SC("sfShape"), -1 );
	sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	sq_register_functions( v, RectangleShape_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
