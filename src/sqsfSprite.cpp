#include "sqsfGlobal.h"
#include "sqsfSprite.h"
#include "sqsfRenderTexture.h"

namespace sqsf {

static SQInteger Sprite_release( SQUserPointer p, SQInteger size ) {
	Sprite* sspr = (Sprite*)p; (void)size;
	delete sspr->spr;
	sq_free( sspr, sizeof (Sprite) );
	return 1;
}

static SQInteger Sprite_construct( HSQUIRRELVM v ) {
	Sprite* sspr = (Sprite*)sq_malloc( sizeof (Sprite) );
	sspr->spr = new sf::Sprite();

	if ( sq_gettop( v ) > 1 ) {
		if ( sq_isinstanceof( v, 2, _SC("sfTexture") ) ) {
			sf::Texture* tex = NULL;
			sq_getinstanceup( v, 2, (SQUserPointer*)&tex, 0 );
			sspr->spr->setTexture( *tex, true );
		} else if ( sq_isinstanceof( v, 2, _SC("sfRenderTexture") ) ) {
			RenderTexture* srtex = NULL;
			sq_getinstanceup( v, 2, (SQUserPointer*)&srtex, 0 );
			sspr->spr->setTexture( srtex->rtex->getTexture(), true );
		} else {
			delete sspr->spr; sq_free( sspr, sizeof (Sprite) );
			return sq_throwerror( v, _SC("Parameter 1 is not a valid instance.") );
		}
	}

	sspr->tfa = (sf::Transformable*)sspr->spr;
	sspr->dra = (sf::Drawable*)sspr->spr;

	sq_setinstanceup( v, 1, sspr );
	sq_setreleasehook( v, 1, Sprite_release );
	return 0;
}

static SQInteger Sprite_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfSprite"), -1 );
	return 1;
}

#define GET_SELF() Sprite* _s = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&_s, 0 ); \
	sf::Sprite* self = _s->spr

static SQInteger Sprite_setTexture( HSQUIRRELVM v ) {
	GET_SELF();
	if ( sq_isinstanceof( v, 2, _SC("sfTexture") ) ) {
		sf::Texture* tex = NULL;
		sq_getinstanceup( v, 2, (SQUserPointer*)&tex, 0 );
		self->setTexture( *tex, true );
	} else if ( sq_isinstanceof( v, 2, _SC("sfRenderTexture") ) ) {
		RenderTexture* srtex = NULL;
		sq_getinstanceup( v, 2, (SQUserPointer*)&srtex, 0 );
		self->setTexture( srtex->rtex->getTexture(), true );
	} else {
		return sq_throwerror( v, _SC("Parameter 1 is not a valid instance.") );
	}

	return 0;
}

static SQInteger Sprite_setColor( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfColor") );
	sf::Color ncol;
	sqsf_getcolor( v, 2, ncol );
	self->setColor( ncol );
	return 0;
}

static SQInteger Sprite_setTextureRect( HSQUIRRELVM v ) {
	GET_SELF();
	VALIDATE_INSTANCEOF( v, 2, _SC("sfRect") );
	sf::IntRect texr;
	sqsf_getrect( v, 2, texr );
	self->setTextureRect( texr );
	return 0;
}

#undef GET_SELF

static SQRegFunction Sprite_RegFunctions[] = {
	{_SC("constructor"), Sprite_construct, -1, _SC(".x")},
	{_SC("_typeof"), Sprite_typeof, 1, _SC("x")},
	{_SC("setTexture"), Sprite_setTexture, 2, _SC("xx")},
	{_SC("setColor"), Sprite_setColor, 2, _SC("xx")},
	{_SC("setTextureRect"), Sprite_setTextureRect, 2, _SC("xx")},
	{0, 0, 0, 0}
};

void Sprite_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfSprite"), -1 );
	sq_pushstring( v, _SC("sfDrawable"), -1 );
	sq_get( v, -3 );
	sq_newclass( v, SQTrue );
	sq_register_functions( v, Sprite_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}
