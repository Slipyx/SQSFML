#ifndef __SQSFVERTEXARRAY_H
#define __SQSFVERTEXARRAY_H

#include <squirrel.h>
#include "sqsfTransformable.h"
#include <SFML/Graphics/VertexArray.hpp>

namespace sqsf {

struct VertexArray : Transformable {
	sf::VertexArray* vxa;
};

void VertexArray_register( HSQUIRRELVM v );

}

#endif
