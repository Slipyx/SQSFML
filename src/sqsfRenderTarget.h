#ifndef __SQSFRENDERTARGET_H
#define __SQSFRENDERTARGET_H

#include <squirrel.h>
#include <SFML/Graphics/RenderTarget.hpp>

namespace sqsf {

struct RenderTarget {
	sf::RenderTarget* rtar;
};

void RenderTarget_register( HSQUIRRELVM v );

}

#endif
