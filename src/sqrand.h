#ifndef __SQRAND_H
#define __SQRAND_H

#include "sqsfGlobal.h"

struct Random {
	int seed;
};

static SQInteger _rndrelease( SQUserPointer p, SQInteger size ) {
	Random* r = (Random*)p; (void)size;
	sq_free( r, sizeof (Random) ); r = 0;
	return 1;
}

static SQInteger sqfunc_rndconstruct( HSQUIRRELVM v ) {
	SQInteger iseed = 0;
	Random* r = NULL;
	sq_getinteger( v, 2, &iseed );

	r = (Random*)sq_malloc( sizeof (Random) );
	r->seed = iseed;

	sq_setinstanceup( v, 1, r );
	sq_setreleasehook( v, 1, _rndrelease );
	return 0;
}

static SQInteger sqfunc_rndtypeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("Random"), -1 );
	return 1;
}

#define GET_SELF() Random* self = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&self, 0 )

static SQInteger sqfunc_rndsetseed( HSQUIRRELVM v ) {
	SQInteger nseed = 0;
	GET_SELF();
	sq_getinteger( v, 2, &nseed );
	self->seed = (int)nseed;
	return 0;
}

static SQInteger sqfunc_rndgetseed( HSQUIRRELVM v ) {
	GET_SELF();
	sq_pushinteger( v, self->seed );
	return 1;
}

static inline void _rndnext( Random* rnd ) {
	rnd->seed = rnd->seed * 0x0BB38435 + 0x3619636B;
}

static SQInteger sqfunc_rndgetn( HSQUIRRELVM v ) {
	GET_SELF();
	_rndnext( self );
	sq_pushinteger( v, abs( self->seed ) );
	return 1;
}

static SQInteger sqfunc_rndgetf( HSQUIRRELVM v ) {
	union { int i; float f; } ifu;
	GET_SELF();
	_rndnext( self );
	ifu.i = ((self->seed & 0x007FFFFF) | 0x3F800000);
	sq_pushfloat( v, ifu.f - 1.0f );
	return 1;
}

#undef GET_SELF

static SQRegFunction rnd_RegFunctions[] = {
	{_SC("constructor"), sqfunc_rndconstruct, 2, _SC(".n")},
	{_SC("_typeof"), sqfunc_rndtypeof, 1, _SC("x")},
	{_SC("setSeed"), sqfunc_rndsetseed, 2, _SC("xn")},
	{_SC("getSeed"), sqfunc_rndgetseed, 1, _SC("x")},
	{_SC("getN"), sqfunc_rndgetn, 1, _SC("x")},
	{_SC("getF"), sqfunc_rndgetf, 1, _SC("x")},
	{0, 0, 0, 0}
};

static void sqrand_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("Random"), -1 );
	sq_newclass( v, SQFalse );
	sq_register_functions( v, rnd_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

#endif
