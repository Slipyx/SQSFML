#ifndef __SQUTIL_H
#define __SQUTIL_H

#include <cstdio>
#include <squirrel.h>
#include "IceKey.h"

void sq_register_functions( HSQUIRRELVM v, SQRegFunction* funcs );

#if 0
// expects _typeof to return string
static void sq_gettypestr( HSQUIRRELVM v, SQInteger idx, const SQChar** type ) {
	sq_typeof( v, idx );
	sq_getstring( v, -1, type );
	sq_pop( v, 1 );
}
#endif

// gets class obj from roottable and checks against instance at idx
SQBool sq_isinstanceof( HSQUIRRELVM v, SQInteger idx, const SQChar* classname );

#define VALIDATE_INSTANCEOF( v, idx, classname ) \
	if ( sq_isinstanceof( v, idx, classname ) != SQTrue ) { \
		SQChar errstr[512]; \
		scsprintf( errstr, 512, _SC("Parameter %d is an invalid instance. Expected '%s'."), \
			idx - 1, classname ); \
		return sq_throwerror( v, errstr ); \
	} \

#endif
