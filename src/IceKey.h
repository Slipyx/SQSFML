/*
** Header file for the C++ ICE encryption class.
** Written by Matthew Kwan - July 1996
*/

#ifndef _ICEKEY_H
#define _ICEKEY_H

class IceSubkey;

class IceKey {
public:
	IceKey( int n = 0 ); // Default thin-ice
	~IceKey();

	void set( const unsigned char* key );
	void encrypt( const unsigned char* plaintext, unsigned char* ciphertext ) const;
	void decrypt( const unsigned char* ciphertext, unsigned char* plaintext ) const;
	int keySize() const { return (_size * 8); }
	int blockSize() const { return (8); }

private:
	void scheduleBuild( unsigned short* k, int n, const int* keyrot );
	int _size;
	int _rounds;
	IceSubkey* _keysched;
};

#endif
