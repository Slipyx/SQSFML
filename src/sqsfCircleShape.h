#ifndef __SQSFCIRCLESHAPE_H
#define __SQSFCIRCLESHAPE_H

//#define SQSF_TFA_TYPE_TAG 0x10000000

#include <squirrel.h>
#include "sqsfShape.h"
#include <SFML/Graphics/CircleShape.hpp>

namespace sqsf {

struct CircleShape : Shape {
	sf::CircleShape* cshape;
};

void CircleShape_register( HSQUIRRELVM v );

}

#endif
