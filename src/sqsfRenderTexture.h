#ifndef __SQSFRENDERTEXTURE_H
#define __SQSFRENDERTEXTURE_H

#include <squirrel.h>
#include "sqsfRenderTarget.h"
#include <SFML/Graphics/RenderTexture.hpp>

namespace sqsf {

struct RenderTexture : RenderTarget {
	sf::RenderTexture* rtex;
};

void RenderTexture_register( HSQUIRRELVM v );

}

#endif
