#include "squtil.h"

void sq_register_functions( HSQUIRRELVM v, SQRegFunction* funcs ) {
	while ( funcs->name != 0 ) {
		sq_pushstring( v, funcs->name, -1 );
		sq_newclosure( v, funcs->f, 0 );
		sq_setparamscheck( v, funcs->nparamscheck, funcs->typemask );
		sq_setnativeclosurename( v, -1, funcs->name );
		sq_newslot( v, -3, SQFalse );
		funcs++;
	}
}

#if 0
// expects _typeof to return string
static void sq_gettypestr( HSQUIRRELVM v, SQInteger idx, const SQChar** type ) {
	sq_typeof( v, idx );
	sq_getstring( v, -1, type );
	sq_pop( v, 1 );
}
#endif

// gets class obj from roottable and checks against instance at idx
SQBool sq_isinstanceof( HSQUIRRELVM v, SQInteger idx, const SQChar* classname ) {
	SQBool res = SQFalse;
	if ( idx < 0 ) idx = sq_gettop( v ) + (idx+1);
	sq_pushroottable( v );
	sq_pushstring( v, classname, -1 ); sq_get( v, -2 );
	sq_push( v, idx );
	res = sq_instanceof( v );
	sq_pop( v, 3 );
	return res;
}
