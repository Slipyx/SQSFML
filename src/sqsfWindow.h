#ifndef __SQSFWINDOW_H
#define __SQSFWINDOW_H

#include <squirrel.h>
#include "sqsfRenderTarget.h"
#include <SFML/Graphics/RenderWindow.hpp>

namespace sqsf {

struct Window : RenderTarget {
	sf::RenderWindow* rwin;
};

void Window_register( HSQUIRRELVM v );

}

#endif
