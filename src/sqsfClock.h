#ifndef __SQSFCLOCK_H
#define __SQSFCLOCK_H

#include <SFML/System/Clock.hpp>

static SQInteger sqsfClock_release( SQUserPointer p, SQInteger size ) {
	sf::Clock* clk = (sf::Clock*)p; (void)size;
	delete clk;
	return 1;
}

static SQInteger sqsfClock_construct( HSQUIRRELVM v ) {
	sf::Clock* clk = NULL;

	clk = new sf::Clock();
	sq_setinstanceup( v, 1, clk );
	sq_setreleasehook( v, 1, sqsfClock_release );
	return 0;
}

static SQInteger sqsfClock_typeof( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfClock"), -1 );
	return 1;
}

#define GET_SELF() sf::Clock* self = NULL; sq_getinstanceup( v, 1, (SQUserPointer*)&self, 0 )

static SQInteger sqsfClock_getElapsedTime( HSQUIRRELVM v ) {
	SQFloat amt = 0;
	GET_SELF();
	amt = self->getElapsedTime().asSeconds();
	sq_pushfloat( v, amt );

	return 1;
}

static SQInteger sqsfClock_restart( HSQUIRRELVM v ) {
	SQFloat amt = 0;
	GET_SELF();
	amt = self->restart().asSeconds();
	sq_pushfloat( v, amt );

	return 1;
}

#undef GET_SELF

static SQRegFunction sqsfClock_RegFunctions[] = {
	{_SC("constructor"), sqsfClock_construct, 1, _SC(".")},
	{_SC("_typeof"), sqsfClock_typeof, 1, _SC("x")},
	{_SC("getElapsedTime"), sqsfClock_getElapsedTime, 1, _SC("x")},
	{_SC("restart"), sqsfClock_restart, 1, _SC("x")},
	{0, 0, 0, 0}
};

namespace sqsf {

static void Clock_register( HSQUIRRELVM v ) {
	sq_pushstring( v, _SC("sfClock"), -1 );
	sq_newclass( v, SQFalse );
	sq_register_functions( v, sqsfClock_RegFunctions );
	sq_newslot( v, -3, SQFalse );
}

}

#endif
