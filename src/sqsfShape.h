#ifndef __SQSFSHAPE_H
#define __SQSFSHAPE_H

#include <squirrel.h>
#include "sqsfTransformable.h"
#include <SFML/Graphics/Shape.hpp>

namespace sqsf {

struct Shape : Transformable {
	sf::Shape* shape;
};

void Shape_register( HSQUIRRELVM v );

}

#endif
