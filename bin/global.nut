enum sfStyle {
	None=0, Titlebar=1, Resize=2, Close=4, Fullscreen=8, Default=7
}

enum sfEvent {
	Closed,Resized,LostFocus,GainedFocus,TextEntered,KeyPressed,KeyReleased,
	MouseWheelMoved,MouseWheelScrolled,MouseButtonPressed,MouseButtonReleased,
	MouseMoved,MouseEntered,MouseLeft,JoystickButtonPressed,JoystickButtonReleased,
	JoystickMoved,JoystickConnected,JoystickDisconnected,TouchBegan,TouchMoved,
	TouchEnded,SensorChanged
}

enum sfKey {
	Unknown=-1,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,
	Num0,Num1,Num2,Num3,Num4,Num5,Num6,Num7,Num8,Num9,Escape,LControl,
	LShift,LAlt,LSystem,RControl,RShift,RAlt,RSystem,Menu,LBracket,
	RBracket,SemiColon,Comma,Period,Quote,Slash,BackSlash,Tilde,Equal,
	Dash,Space,Return,BackSpace,Tab,PageUp,PageDown,End,Home,Insert,
	Delete,Add,Subtract,Multiply,Divide,Left,Right,Up,Down,Numpad0,
	Numpad1,Numpad2,Numpad3,Numpad4,Numpad5,Numpad6,Numpad7,Numpad8
	Numpad9,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,F13,F14,F15,Pause
}

enum sfMouseButton {
	Left, Right, Middle, XButton1, XButton2, ButtonCount
}

enum sfPrimitiveType {
	Points, Lines, LineStrip, Triangles, TriangleStrip, TriangleFan, Quads
}

enum sfSocket {
	Done, NotReady, Partial, Disconnected, Error
}

class sfVideoMode {
	width = null; height = null; bitsPerPixel = null;

	constructor( w, h, bpp=32 ) {
		width = w; height = h; bitsPerPixel = bpp;
	}

	_typeof=@()"sfVideoMode";
	_tostring=@()"["+width+"x"+height+"@"+bitsPerPixel+"]";
}

class sfColor {
	r=null; g=null; b=null; a = null;

	constructor( rc, gc, bc, ac=255 ) {
		r=rc;g=gc;b=bc; a=ac;
	}

	_typeof=@()"sfColor";
	_tostring=@()"(" + r + ", " + g + ", " + b + ", " + a + ")";
}

sfColor.newmember( "Black", sfColor(0,0,0),null,true);
sfColor.newmember( "White", sfColor(255,255,255),null,true);
sfColor.newmember( "Red", sfColor(255,0,0),null,true);
sfColor.newmember( "Green", sfColor(0,255,0),null,true);
sfColor.newmember( "Blue", sfColor(0,0,255),null,true);
sfColor.newmember( "Yellow", sfColor(255,255,0),null,true);
sfColor.newmember( "Transparent", sfColor(0,0,0,0),null,true);
sfColor.newmember( "Orange", sfColor(255,127,0),null,true);
sfColor.newmember( "Magenta", sfColor(255,0,255),null,true);

//class sfRenderTarget { function draw() {} _typeof=@()"sfRenderTarget"; }

class sfVec2 {
	x=null;y=null;
	constructor( xc, yc=null ) {
		x = xc;
		y = (yc == null) ? xc : yc;
	}
	_typeof=@()"sfVec2";
	_tostring=@()"("+x+", "+y+")";
	function _mul(o) {
		if ( typeof o == "integer" || typeof o == "float" ) return sfVec2(x*o,y*o);
		else if ( typeof o == "sfVec2" ) return sfVec2( x*o.x, y*o.y );
		else throw "Unsupported type for sfVec2 multiply.";
	}
	function length() {
		return sqrt( x*x + y*y );
	}
	function normalize() {
		local len = length();
		x /= len; y /= len;
	}
	function normalized() {
		local len = length();
		return sfVec2( x / len, y / len );
	}
}

sfVec2.newmember( "Zero", sfVec2(0), null, true );
sfVec2.newmember( "One", sfVec2(1), null, true );
sfVec2.newmember( "Right", sfVec2(1,0), null, true );
sfVec2.newmember( "Down", sfVec2(0,1), null, true );

class sfRect {
	left=null;top=null;width=null;height=null;

	constructor( l, t, w, h ) { left=l;top=t;width=w;height=h }

	_typeof=@()"sfRect";
	_tostring=@()"("+left+", "+top+", "+width+", "+height+")";
}

function _VIO(ix,i,c) {
	if ( !(i instanceof c) ) {
		throw format( "Parameter %u is not a valid class instance.", ix );
	}
}

class sfVertex {
	position = null; color = null; texCoords = null;

	constructor( pos=sfVec2(0,0), col=sfColor.White, tc=sfVec2(0,0) ) {
		_VIO(1,pos,sfVec2); _VIO(2,col,sfColor); _VIO(3,tc,sfVec2);
		position = pos; color = col; texCoords = tc;
	}

	_typeof=@()"sfVertex";
	_tostring=@()"["+position+color+texCoords+"]";
}
