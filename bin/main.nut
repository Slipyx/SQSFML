// Global random generator
random <- Random( time() );
random.getN(); // generate

// Some video modes
local vidMode = sfVideoMode( 1024, 576 );
local dtVidMode = sfVideoMode.getDesktopMode();
// Global window object
window <- sfWindow( vidMode, "dubious", sfStyle.Default );
window.setVerticalSyncEnabled( true );
//window.setFramerateLimit( 30 );

// Font and text
local font0 = sfFont( "fonts/CharterBT-Roman.ttf" );
local text0 = sfText( "hello there", font0, 16 );
text0.setPosition( sfVec2( vidMode.width/2, 16 ) );
text0.setOrigin( sfVec2( 0, 4 ) );

// Texture
local tex0 = sfTexture( "WALL.png" );
tex0.setSmooth( true );
tex0.setRepeated( true );

// Shapes
local rshape = array( 200 );
for ( local i = 0; i < rshape.len(); ++i ) {
	rshape[i] = sfSprite( tex0 );
	rshape[i].setOrigin( sfVec2( 8, 8 ) );
	rshape[i].setTextureRect( sfRect( 0, 0, 16, 16 ) );
	rshape[i].setPosition( sfVec2( random.getN() % vidMode.width,random.getN()%vidMode.height ) );
	rshape[i].setRotation( random.getN() % 360 );
	rshape[i].setColor( sfColor( random.getN() % 255, random.getN() % 255, random.getN() % 255, 127 ) );
}
local scf = 1;
local cshape = sfCircleShape( 64, 16 );
cshape.setPosition( sfVec2( 100 ) );
cshape.setOutlineThickness( 1 );
cshape.setOutlineColor( sfColor.White );
cshape.setFillColor( sfColor.Transparent );

// Clocks
local dtclk = sfClock();
local dtxc = sfClock();
local dttxt = 0.0;

while ( window.isOpen() ) {
	local dt = dtclk.restart();
	local event = {}; // empty table to be filled with current event info
	while ( window.pollEvent( event ) ) {
		if ( event.type == sfEvent.Closed ) {
			window.close();
		}
		if ( event.type == sfEvent.KeyReleased ) {
			if ( event.key.code == sfKey.F12 ) window.close();
		}
		if ( event.type == sfEvent.MouseMoved ) {
			cshape.setPosition( event.mouseMove );
		}
	}

	if ( dtxc.getElapsedTime() >= 0.125 ) {
		dttxt = dt;
		dtxc.restart();
	}

	foreach ( rs in rshape ) {
		scf = ((sin( clock()*4 ) + 1) * 0.5) + 1;
		rs.setScale( sfVec2.One * scf );
		rs.setRotation( rs.getRotation() + 60 * dt );
	}
	cshape.setRadius( 64 * fabs( sin( clock() * 2 ) ) );
	cshape.setOrigin( sfVec2( cshape.getRadius() ) );

	text0.setString( format( "DT: %.2fms (%.2ffps) %f", dttxt*1000, 1/dttxt,
		fabs(sin(clock())) ) );
	text0.setOrigin( sfVec2( text0.getLocalBounds().width/2, 0 ) );

	// Drawing
	window.clear( sfColor( 32, 32, 32 ) );

	foreach ( rs in rshape )
		window.draw( rs );
	window.draw( cshape );

	window.draw( text0 );

	window.display();
}

print( window.isOpen()+"\n" );
print( random.getF() );
